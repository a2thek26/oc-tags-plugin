<?php namespace Devinci\Tags\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Devinci\Tags\Models\Tag;

/**
 * TagField Form Widget
 */
class TagField extends FormWidgetBase
{

    /**
     * {@inheritDoc}
     */
    protected $defaultAlias = 'devinci_tags_tag_field';

	public function widgetDetails()
	{
		return [
			'name'        => 'Tag Field',
		    'description' => 'Renders a tag field.'
		];
	}

    /**
     * {@inheritDoc}
     */
    public function init()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('tagfield');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['name']  = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['model'] = $this->model;
	    $this->vars['tags']  = Tag::lists('name', 'id');
    }

    /**
     * {@inheritDoc}
     */
    public function loadAssets()
    {
        $this->addCss('/modules/backend/assets/vendor/select2/select2.css');
        $this->addCss('/modules/backend/assets/vendor/select2/select2-bootstrap.css');
        $this->addCss('css/tagfield.css', 'Devinci.Tags');
	    $this->addJs('/modules/backend/assets/vendor/select2/select2.js');
        $this->addJs('js/tagfield.js', 'Devinci.Tags');
    }

    /**
     * {@inheritDoc}
     */
    public function getSaveValue($value)
    {
        return $value;
    }

}
