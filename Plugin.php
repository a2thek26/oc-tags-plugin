<?php namespace Devinci\Tags;

use Backend;
use System\Classes\PluginBase;

/**
 * Tags Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Tags',
            'description' => 'You know what tags do...',
            'author'      => 'Devinci',
            'icon'        => 'icon-tag'
        ];
    }

	/**
	 * @return array
	 */
	public function registerNavigation()
	{
		return [
			'tags' => [
				'label'       => 'devinci.tags::lang.tag.menu_label',
				'url'         => Backend::url('devinci/tags/tags'),
				'icon'        => 'icon-tag',
				'permissions' => ['devinci.tags.*'],
				'order'       => 500,
		    ]
		];
	}

	/**
	 * @return array
	 */
	public function registerFormWidgets()
	{
		return [
			'Devinci\Tags\FormWidgets\TagField' => [
				'label' => 'Tag Field',
			    'code'  => 'devinci_tags_tag_field',
			]
		];
	}

}
