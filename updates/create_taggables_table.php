<?php namespace Devinci\Tags\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTaggablesTable extends Migration
{

    public function up()
    {
        Schema::create('devinci_tags_taggables', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
	        $table->unsignedInteger('tag_id');
	        $table->unsignedInteger('taggable_id');
	        $table->string('taggable_type');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('devinci_tags_taggables');
    }

}
