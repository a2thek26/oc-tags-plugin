<?php namespace Devinci\Tags\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTagsTable extends Migration
{

    public function up()
    {
        Schema::create('devinci_tags_tags', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
	        $table->string('name');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('devinci_tags_tags');
    }

}
